use rand::Rng;
use std::cmp::Ordering;
use std::io;
use std::process;

fn set_numbers() -> (i32,i32) {
    let mut lower_limit = String::new();
    println!("Choose the lower limit: ");
    io::stdin()
        .read_line(&mut lower_limit)
        .expect("Failed to read value");
    let lower_limit: i32 = match lower_limit.trim().parse(){
        Ok(num) => num,
        Err(_) => 1,
    };
    
    let mut upper_limit = String::new();
    println!("Choose the upper limit: ");
    io::stdin()
        .read_line(&mut upper_limit)
        .expect("Failed to read value");
    let upper_limit: i32 = match upper_limit.trim().parse(){
        Ok(num) => num,
        Err(_) => 1,
    };
    if lower_limit >=  upper_limit
    {
    println!("What kind of range is that?! {} is not smaller than {}. Return when you can tell what when a number is bigger or smaller than the other.", lower_limit, upper_limit);
    process::exit(1);
    }
    println!("Range set to between {} and {}.",lower_limit,upper_limit);
    return (lower_limit,upper_limit);
}

fn guess_game(lower_limit: i32,upper_limit: i32) {
    println!("The game starts now, input your guess!");
    let range = lower_limit..upper_limit;
    let winningnumber = rand::thread_rng().gen_range(lower_limit..upper_limit);
    loop {

        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line, are you trying something fishy?");

        let guess: i32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        if range.contains(&guess){
            match guess.cmp(&winningnumber) {
                Ordering::Less => println!("Too small, try higher!"),
                Ordering::Greater => println!("Too big, try lower!"),
                Ordering::Equal => {
                    println!("Congratulations, you win! The winning number was {}.", winningnumber);
                    break;
                }
            }
        } else 
        {
        println!("Hey, {} is not between {} and {}. Try again.",guess,lower_limit,upper_limit);
        };
    }
}

fn main() {
    loop {
        println!("Guess the number - The Game for real Gamers");
        let (lower_limit,upper_limit) = set_numbers();
        guess_game(lower_limit,upper_limit);
        let mut play_again = String::new();
        println!("Want to play again? yes/no");
        io::stdin()
            .read_line(&mut play_again)
            .expect("Failed to read value");
        let play_again = play_again
            .strip_suffix("\r\n")
            .or(play_again.strip_suffix("\n"))
            .unwrap_or(&play_again);
        match play_again == "yes" {
            true => println!("Let's go!"),
            _ => { println!("Goodbye!"); break}
        }
    };
}
