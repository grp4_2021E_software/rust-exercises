
# Rust-exercises
## This repository is part of some exercises for a course called Software security. 

To build the software and get an exacutable, run cargo build in the specific exercise directory.  

https://doc.rust-lang.org/stable/book/

### Exercise 0
Install rust and read Chapter 1 getting started   
Do Hello World in Rust  

### Exercise 1
Guessing game written in Chapter 2  

### Exercise 2
Read Chapter 4 and explain ownership  

### Exercise 3
Make webserver as explained in Chapter 20  
 
### Exercise 4
Try and do an AES encryption decryption to make a software library??   
